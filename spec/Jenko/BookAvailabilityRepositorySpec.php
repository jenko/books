<?php

namespace spec\Jenko;

use Jenko\BookAvailability;
use Jenko\BookAvailabilityId;
use Jenko\EventStore\InMemoryEventStore;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BookAvailabilityRepositorySpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedWith(new InMemoryEventStore());
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Jenko\BookAvailabilityRepository');
    }

    function it_should_reconstitute_book_availability_to_its_state_after_persisting_it()
    {
        $bookAvailabilityId = BookAvailabilityId::generate();
        $bookAvailability = BookAvailability::create($bookAvailabilityId);

        $this->add($bookAvailability);

        $this->get($bookAvailabilityId)->shouldBeLike($bookAvailability);
    }
}

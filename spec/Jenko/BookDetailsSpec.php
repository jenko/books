<?php

namespace spec\Jenko;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BookDetailsSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedThrough('named', ['foo']);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Jenko\BookDetails');
    }

    function it_should_be_created_with_name()
    {
        $book = self::named('foo');
        $book->shouldHaveType('Jenko\BookDetails');
        $book->getName()->shouldEqual('foo');
    }
}

<?php

namespace spec\Jenko;

use Jenko\BookDetailsId;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BookDetailsIdSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedThrough('fromString', ['1234']);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Jenko\BookDetailsId');
    }

    function it_should_be_created_from_string()
    {
        $book = self::fromString('1234');
        $book->shouldHaveType('Jenko\BookDetailsId');
        $book->__toString()->shouldEqual('1234');

    }

    function it_should_compare_id_for_equality()
    {
        $nextBookId = BookDetailsId::fromString('1235');
        $this->equals($nextBookId)->shouldEqual(false);

        $sameBookId = BookDetailsId::fromString('1234');
        $this->equals($sameBookId)->shouldEqual(true);
    }

    function  it_should_create_a_new_instance_with_generated_id()
    {
        $book = self::generate();
        $book->shouldHaveType('Jenko\BookDetailsId');
    }
}

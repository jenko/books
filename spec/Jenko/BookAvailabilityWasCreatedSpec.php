<?php

namespace spec\Jenko;

use Jenko\BookAvailabilityId;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BookAvailabilityWasCreatedSpec extends ObjectBehavior
{
    function let()
    {
        $bookAvailabilityId = BookAvailabilityId::generate();
        $this->beConstructedWith($bookAvailabilityId);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Jenko\BookAvailabilityWasCreated');
    }
}

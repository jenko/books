<?php

namespace spec\Jenko;

use Jenko\BookAvailabilityId;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BookAvailabilityIdSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedThrough('fromString', ['1234']);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Jenko\BookAvailabilityId');
    }

    function it_should_be_created_from_string()
    {
        $bookAvailability = self::fromString('1234');
        $bookAvailability->shouldHaveType('Jenko\BookAvailabilityId');
        $bookAvailability->__toString()->shouldEqual('1234');

    }

    function it_should_compare_id_for_equality()
    {
        $nextBookAvailabilityId = BookAvailabilityId::fromString('1235');
        $this->equals($nextBookAvailabilityId)->shouldEqual(false);

        $sameBookAvailabilityId = BookAvailabilityId::fromString('1234');
        $this->equals($sameBookAvailabilityId)->shouldEqual(true);
    }

    function  it_should_create_a_new_instance_with_generated_id()
    {
        $book = self::generate();
        $book->shouldHaveType('Jenko\BookAvailabilityId');
    }
}

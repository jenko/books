<?php

namespace spec\Jenko;

use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class LoanIdSpec extends ObjectBehavior
{
    function it_is_initializable()
    {
        $this->shouldHaveType('Jenko\LoanId');
    }
}

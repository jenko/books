<?php

namespace spec\Jenko;

use Buttercup\Protects\AggregateHistory;
use Jenko\BookAvailability;
use Jenko\BookAvailabilityId;
use Jenko\LoanId;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BookAvailabilitySpec extends ObjectBehavior
{
    function let()
    {
        $bookAvailabilityId = BookAvailabilityId::generate();
        $this->beConstructedThrough('create', [$bookAvailabilityId]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Jenko\BookAvailability');
    }

    function it_should_allow_checking_out_of_available_books()
    {
        $this->shouldNotThrow('Jenko\BookAlreadyCheckedOutException')->during(
            'loanBook',
            [LoanId::fromString('IJ1')]
        );
    }

    function it_should_not_allow_checking_out_of_a_book_already_checked_out()
    {
        $this->loanBook(LoanId::fromString('IJ1'));

        $this->shouldThrow('Jenko\BookAlreadyCheckedOutException')->during(
            'loanBook',
            [LoanId::fromString('IJ2')]
        );
    }

    function it_should_not_allow_checking_out_of_a_removed_book()
    {
        $this->loanBook(LoanId::fromString('IJ1'));

        $this->shouldThrow('Jenko\BookAlreadyCheckedOutException')->during(
            'loanBook',
            [LoanId::fromString('IJ2')]
        );
    }

    function it_should_be_the_same_after_reconsitution()
    {
        $bookAvailabilityId = BookAvailabilityId::generate();
        $bookAvailability = BookAvailability::create($bookAvailabilityId);

        $bookAvailability->loanBook(LoanId::fromString('IJ1'));

        $events = $bookAvailability->getRecordedEvents();
        $bookAvailability->clearRecordedEvents();

        $reconstitutedBookAvailability = BookAvailability::reconstituteFrom(
            new AggregateHistory($bookAvailabilityId, (array) $events)
        );

        if ($reconstitutedBookAvailability != $bookAvailability) {
            throw new \Exception();
        }
    }
}

<?php

namespace spec\Jenko;

use Jenko\BookAvailabilityId;
use Jenko\LoanId;
use Jenko\Status;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class BookWasLoanedSpec extends ObjectBehavior
{
    function let()
    {
        $bookAvailabilityId = BookAvailabilityId::generate();
        $loanId = LoanId::generate();
        $this->beConstructedWith($bookAvailabilityId, $loanId, Status::named(Status::AVAILABLE));
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Jenko\BookWasLoaned');
    }
}

<?php

namespace spec\Jenko;

use Jenko\Status;
use PhpSpec\ObjectBehavior;
use Prophecy\Argument;

class StatusSpec extends ObjectBehavior
{
    function let()
    {
        $this->beConstructedThrough('named', [Status::AVAILABLE]);
    }

    function it_is_initializable()
    {
        $this->shouldHaveType('Jenko\Status');
    }

    function it_should_be_created_with_name()
    {
        $status = self::named(Status::AVAILABLE);
        $status->shouldHaveType('Jenko\Status');
        $status->getName()->shouldEqual(Status::AVAILABLE);
    }


    function it_should_compare_name_for_equality()
    {
        $newStatus = Status::named(Status::CHECKED_OUT);
        $this->equals($newStatus)->shouldEqual(false);

        $newerStatus = Status::named(Status::AVAILABLE);
        $this->equals($newerStatus)->shouldEqual(true);
    }
}

<?php

namespace Jenko;

use Buttercup\Protects\IdentifiesAggregate;

final class BookAvailabilityId implements IdentifiesAggregate
{
    /**
     * @var string
     */
    private $bookAvailabilityId;

    /**
     * BookAvailabilityId constructor.
     * @param string $id
     */
    private function __construct($id)
    {
        $this->bookAvailabilityId = $id;
    }

    /**
     * @param string $id
     * @return BookAvailabilityId
     */
    public static function fromString($id)
    {
        return new BookAvailabilityId($id);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->bookAvailabilityId;
    }

    /**
     * @param IdentifiesAggregate $other
     * @return bool
     */
    public function equals(IdentifiesAggregate $other)
    {
        return $other instanceof BookAvailabilityId && $this->bookAvailabilityId == $other->bookAvailabilityId;
    }

    /**
     * @return BookAvailabilityId
     */
    public static function generate()
    {
        // TODO: Swap to proper uuid lib
        $badSampleUuid = md5(uniqid());
        return new BookAvailabilityId($badSampleUuid);
    }
}

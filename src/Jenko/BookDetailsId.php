<?php

namespace Jenko;

use Buttercup\Protects\IdentifiesAggregate;

final class BookDetailsId implements IdentifiesAggregate
{
    /**
     * @var string
     */
    private $bookDetailsId;

    /**
     * BookDetailsId constructor.
     * @param string $id
     */
    private function __construct($id)
    {
        $this->bookDetailsId = $id;
    }

    /**
     * @param string $id
     * @return BookDetailsId
     */
    public static function fromString($id)
    {
        return new BookDetailsId($id);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->bookDetailsId;
    }

    /**
     * @param IdentifiesAggregate $other
     * @return bool
     */
    public function equals(IdentifiesAggregate $other)
    {
        return $other instanceof BookDetailsId && $this->bookDetailsId == $other->bookDetailsId;
    }

    /**
     * @return BookDetailsId
     */
    public static function generate()
    {
        // TODO: Swap to use proper uuid lib
        $badSampleUuid = md5(uniqid());
        return new BookDetailsId($badSampleUuid);
    }
}

<?php

namespace Jenko;

use Buttercup\Protects\DomainEvent;

final class BookAvailabilityWasCreated implements DomainEvent
{
    /**
     * @var BookAvailabilityId
     */
    private $bookAvailabilityId;

    /**
     * BookAvailabilityWasCreated constructor.
     * @param BookAvailabilityId $bookAvailabilityId
     */
    public function __construct(BookAvailabilityId $bookAvailabilityId)
    {
        $this->bookAvailabilityId = $bookAvailabilityId;
    }

    /**
     * @return BookAvailabilityId
     */
    public function getAggregateId()
    {
        return $this->bookAvailabilityId;
    }
}

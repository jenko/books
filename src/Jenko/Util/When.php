<?php

namespace Jenko\Util;

use Buttercup\Protects\DomainEvent;

trait When
{
    protected function when(DomainEvent $event)
    {
        $reflected = new \ReflectionClass(get_class($event));

        $method = 'when' . $reflected->getShortName();
        $this->$method($event);
    }
}

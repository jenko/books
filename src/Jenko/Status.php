<?php

namespace Jenko;

final class Status
{
    /**
     * @var string
     */
    const AVAILABLE = 'available';
    const CHECKED_OUT = 'checked out';
    const REMOVED = 'removed';

    /**
     * @var string
     */
    private $name;

    /**
     * @param string $name
     */
    private function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $name
     * @return Status
     */
    public static function named($name)
    {
        $status = new Status($name);

        return $status;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param Status $status
     * @return bool
     */
    public function equals(Status $status)
    {
        if ($status->getName() === $this->getName()) {
            return true;
        }

        return false;
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->getName();
    }
}

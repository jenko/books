<?php

namespace Jenko;

use Buttercup\Protects\AggregateRepository;
use Buttercup\Protects\IdentifiesAggregate;
use Buttercup\Protects\IsEventSourced;
use Buttercup\Protects\RecordsEvents;
use Jenko\EventStore\EventStore;

final class BookAvailabilityRepository implements AggregateRepository
{
    /**
     * @var EventStore
     */
    private $eventStore;

    /**
     * BookAvailabilityRepository constructor.
     * @param EventStore $eventStore
     */
    public function __construct(EventStore $eventStore)
    {
        $this->eventStore = $eventStore;
    }

    /**
     * @param IdentifiesAggregate $aggregateId
     * @return IsEventSourced
     */
    public function get(IdentifiesAggregate $aggregateId)
    {
        $aggregateHistory = $this->eventStore->getAggregateHistoryFor($aggregateId);
        return BookAvailability::reconstituteFrom($aggregateHistory);
    }

    /**
     * @param RecordsEvents $aggregate
     * @return void
     */
    public function add(RecordsEvents $aggregate)
    {
        $events = $aggregate->getRecordedEvents();
        $this->eventStore->commit($events);
        $aggregate->clearRecordedEvents();
    }
}

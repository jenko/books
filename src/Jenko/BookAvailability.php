<?php

namespace Jenko;

use Buttercup\Protects\AggregateHistory;
use Buttercup\Protects\DomainEvent;
use Buttercup\Protects\DomainEvents;
use Buttercup\Protects\IsEventSourced;
use Buttercup\Protects\RecordsEvents;
use Jenko\Util\When;

final class BookAvailability implements RecordsEvents, IsEventSourced
{
    use When;

    /**
     * @var BookAvailabilityId
     */
    private $bookAvailabilityId;

    /**
     * @var Status
     */
    private $currentStatus;

    /**
     * @var DomainEvent[]
     */
    private $latestRecordedEvents = [];

    /**
     * BookAvailability constructor.
     * @param BookAvailabilityId $bookAvailabilityId
     */
    private function __construct(BookAvailabilityId $bookAvailabilityId)
    {
        $this->bookAvailabilityId = $bookAvailabilityId;
    }

    /**
     * @param BookAvailabilityId $bookAvailabilityId
     * @return BookAvailability
     */
    public static function create(BookAvailabilityId $bookAvailabilityId)
    {
        $bookAvailability = new BookAvailability($bookAvailabilityId);

        $bookAvailability->recordThat(
            new BookAvailabilityWasCreated($bookAvailabilityId)
        );

        return $bookAvailability;
    }

    /**
     * @param LoanId $loanId
     * @throws BookAlreadyCheckedOutException
     */
    public function loanBook(LoanId $loanId)
    {
        // Cannot check out a book that's currently checked out
        if ($this->currentStatus->equals(Status::named(Status::CHECKED_OUT))
        ) {
            throw new BookAlreadyCheckedOutException();
        }

        // Cannot check out a book that's currently removed
        if ($this->currentStatus->equals(Status::named(Status::REMOVED))
        ) {
            // TODO: Use better exception
            throw new BookAlreadyCheckedOutException();
        }

        $this->recordThat(
            new BookWasLoaned($this->bookAvailabilityId, $loanId, Status::named(Status::CHECKED_OUT))
        );
    }

    /**
     * @return bool
     */
    public function available()
    {
        return $this->currentStatus->equals(Status::named(Status::AVAILABLE));
    }

    /**
     * @param DomainEvent $domainEvent
     */
    private function recordThat(DomainEvent $domainEvent)
    {
        $this->latestRecordedEvents[] = $domainEvent;

        $this->when($domainEvent);
    }

    /**
     * @return DomainEvents
     */
    public function getRecordedEvents()
    {
        return new DomainEvents($this->latestRecordedEvents);
    }

    /**
     * @return void
     */
    public function clearRecordedEvents()
    {
        $this->latestRecordedEvents = [];
    }

    /**
     * @param AggregateHistory $aggregateHistory
     * @return RecordsEvents
     */
    public static function reconstituteFrom(AggregateHistory $aggregateHistory)
    {
        /** @var BookAvailabilityId $bookAvailabilityId */
        $bookAvailabilityId = $aggregateHistory->getAggregateId();

        $bookAvailability = new BookAvailability($bookAvailabilityId);

        foreach($aggregateHistory as $event) {
            $bookAvailability->when($event);
        }

        return $bookAvailability;
    }

    /**
     * @param BookAvailabilityWasCreated $event
     */
    private function whenBookAvailabilityWasCreated(BookAvailabilityWasCreated $event)
    {
        $this->currentStatus = Status::named(Status::AVAILABLE);
    }

    /**
     * @param BookWasLoaned $event
     */
    private function whenBookWasLoaned(BookWasLoaned $event)
    {
        $this->currentStatus = $event->getStatus();
    }
}

<?php

namespace Jenko;

use Buttercup\Protects\IdentifiesAggregate;

final class LoanId implements IdentifiesAggregate
{
    /**
     * @var string
     */
    private $loanId;

    /**
     * LoanId constructor.
     * @param string $id
     */
    private function __construct($id)
    {
        $this->loanId = $id;
    }

    /**
     * @param string $id
     * @return LoanId
     */
    public static function fromString($id)
    {
        return new LoanId($id);
    }

    /**
     * @return string
     */
    public function __toString()
    {
        return $this->loanId;
    }

    /**
     * @param IdentifiesAggregate $other
     * @return bool
     */
    public function equals(IdentifiesAggregate $other)
    {
        return $other instanceof LoanId && $this->loanId == $other->loanId;
    }

    /**
     * @return LoanId
     */
    public static function generate()
    {
        // TODO: Swap to proper uuid lib
        $badSampleUuid = md5(uniqid());
        return new LoanId($badSampleUuid);
    }
}

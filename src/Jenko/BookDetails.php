<?php

namespace Jenko;

final class BookDetails
{
    /**
     * @var string
     */
    private $name;

    /**
     * BookDetails constructor.
     * @param string $name
     */
    private function __construct($name)
    {
        $this->name = $name;
    }

    /**
     * @param string $name
     * @return BookDetails
     */
    public static function named($name)
    {
        return new BookDetails($name);
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }
}

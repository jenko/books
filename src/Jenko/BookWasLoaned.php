<?php

namespace Jenko;

use Buttercup\Protects\DomainEvent;

final class BookWasLoaned implements DomainEvent
{
    /**
     * @var BookAvailabilityId
     */
    private $bookAvailabilityId;

    /**
     * @var LoanId
     */
    private $loanId;

    /**
     * @var Status
     */
    private $status;

    /**
     * LoanWasAddedToBookAvailability constructor.
     * @param BookAvailabilityId $bookAvailabilityId
     * @param LoanId $loanId
     * @param Status $status
     */
    public function __construct(BookAvailabilityId $bookAvailabilityId, LoanId $loanId, Status $status)
    {
        $this->bookAvailabilityId = $bookAvailabilityId;
        $this->loanId = $loanId;
        $this->status = $status;
    }

    /**
     * @return BookAvailabilityId
     */
    public function getAggregateId()
    {
        return $this->bookAvailabilityId;
    }

    /**
     * @return LoanId
     */
    public function getLoanId()
    {
        return $this->loanId;
    }

    /**
     * @return Status
     */
    public function getStatus()
    {
        return $this->status;
    }
}

# Books

Pissing about with books to try and get my head around https://groups.google.com/d/msg/dddinphp/OKC9LuNEIoY/reuvb3laAQAJ

Use [Buttercup-Protects](https://github.com/buttercup-php/protects) library to help put it all together.

## Basic idea

A book consists of the details of a book (BookDetails) and the availability of the book (BookAvailability). 
BookDetails is just metadata stored about a book, such as name, description etc. (just name currently for brevity).
The interesting stuff happens within BookAvailability and thus this is our Aggregate Root. 

BookDetails will just get stored in a database doing nothing greatly interesting and would typically be managed using the typical CRUD type feature.
BookAvailability has some distinct events. Currently:

`BookAvailabilityWasCreated`

This happens whenever a new availability slot is created. Typically this would be when a new book gets added, or similarly when a new copy of a book comes in.

`BookWasLoaned`

This happens when a book is loaned out.

## Tests

### PHPSpec

```php
vendor/bin/phpspec run
```

### Behat

```php
vendor/bin/behat
```


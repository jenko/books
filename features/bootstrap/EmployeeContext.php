<?php

use Behat\Behat\Context\Context;
use Behat\Behat\Context\SnippetAcceptingContext;
use Jenko\BookDetails;
use Jenko\BookAvailability;

class EmployeeContext implements Context, SnippetAcceptingContext
{
    private $bookName;
    private $book;
    private $bookAvailability;

    /**
     * @Given there is a book called :bookName
     */
    public function thereIsABookCalled($bookName)
    {
        $this->bookName = $bookName;
    }

    /**
     * @When I create the book
     */
    public function iCreateTheBook()
    {
        $this->book = BookDetails::named($this->bookName);
    }

    /**
     * @When make it available
     */
    public function makeItAvailable()
    {
        $this->bookAvailability = BookAvailability::create(\Jenko\BookAvailabilityId::generate());
    }

    /**
     * @Then the book should be available for loaning
     */
    public function theBookShouldBeAvailableForLoaning()
    {
        assert(true === $this->bookAvailability->available());
    }
}

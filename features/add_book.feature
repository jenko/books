Feature: Create a new book
  In order to make a book available for checkout
  As an employee
  I want to create a book and make it available

  Scenario: Creating a new book
    Given there is a book called "Deal with it"
    When I create the book
    And make it available
    Then the book should be available for loaning
